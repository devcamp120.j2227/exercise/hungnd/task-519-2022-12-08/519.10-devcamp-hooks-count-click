
import './App.css';

import ClickCount from "./Components/ClickCount";

function App() {
  return (
    <div className="container">
      <ClickCount/>
    </div>
  );
}

export default App;
