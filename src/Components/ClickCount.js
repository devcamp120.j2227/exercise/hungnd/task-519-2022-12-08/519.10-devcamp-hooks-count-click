import {useEffect, useState} from "react";

const  ClickCount = () => {
    const [count, setCount] = useState(0);

    const onBtnClick = () => {
        setCount(count + 1);
    }

    //sự kiện xảy ra 1 lần: componentDidMount
    useEffect(()=>{
        console.log("Use Effect as componentDidMount")
        document.title = `You Clicked ${count} times`
    },[])   // <-- chỗ này

    //sự kiện xảy ra 1 lần: componentDidMount (Được gọi khi biến count thay đổi)
    useEffect(()=>{
        console.log("Use Effect componentDidMount + componentDidUpdate")
        document.title = `You Clicked ${count} times`
    },[count])

    //sự kiện xảy ra mỗi khi render lại component: componentDidUpdate (Luôn được gọi)
    useEffect(()=>{
        document.title = `You Clicked ${count} times`;
        return ()=>{
            //sự kiện xảy ra khi component biến mất: componentWillUnmount
            console.log("ComponentWillUnmount...");
        }
    })   // <--  khác chỗ này
    return(
        <div className="div-main">
            <p>You Clicked {count} times</p>
            <button onClick={onBtnClick}>Click Me</button>
        </div>
    )
}
export default ClickCount;